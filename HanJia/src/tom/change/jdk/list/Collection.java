package tom.change.jdk.list;

public interface Collection<E> extends Iterable<E>{

	/**
	 * 返回集合中的数量，如何返回的数量大于Integer.MAX_VALUE，则返回Integer.MAX_VALUE，
	 * Integer.MAX_VALUE=0x7fffffff=(2^31)-1
	 * @return
	 */
	int size();
	/**
	 * 如果集合中没有元素返回true
	 * @return
	 */
	boolean isEmpty();
	/**
	 * 如果集合包含制定的值则返回true,
	 * @param e
	 * @return
	 */
	boolean contains(Object e);
	/**
	 * 添加元素
	 * @param e
	 * @return
	 */
	boolean add(E e);
	
}
