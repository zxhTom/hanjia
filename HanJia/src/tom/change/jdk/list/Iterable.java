package tom.change.jdk.list;

public interface Iterable<T> {

	/**
	 * 返回iterator覆盖set类型T
	 * 
	 * @return an iterator
	 */
	Iterable<T> iterable();
}
