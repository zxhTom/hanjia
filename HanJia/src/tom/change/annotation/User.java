package tom.change.annotation;

/**
 * 测试所有类型的注解的作用
 * @author xinhua
 *
 */
public @TypeAnno(desc="我是普通类")class User
{
    @FieldAnno
    private String UID;
    
    @FieldAnno(desc="我是UserName的")
    private String userName;
    
    @ConstructorAnno
    public User(){
        
    }
    
    @MethodAnno(time="2015-12-8" , sex=false)
    public void doHomeWork(@StuInfo(value="211311084") String UID, @StuInfo(value="张新华")String UserName){
         @LocalAnno(desc="flag的局部变量")boolean flag;
         
    }
}
