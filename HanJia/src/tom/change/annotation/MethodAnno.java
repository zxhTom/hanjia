package tom.change.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 这个注解用于生成doc文档
 * 这个注解用于类的方法上的注解
 * 这个注解尽在运行期间有效
 * @author xinhua
 *
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MethodAnno
{
    //默认提交时间  2017-2-28
    String time() default "2017-2-28";
    
    //默认  男    true 男  false 女 
    boolean sex() default true;
}
