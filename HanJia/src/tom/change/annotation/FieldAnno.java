package tom.change.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.Documented;

/**
 * 该注解用于生成doc文档
 * 该注解用于在类的字段属性上的注解
 * 该注解尽在运行期间有效
 * @author xinhua
 *
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldAnno
{

    String desc() default "我是描述字段的注解";
}
