package tom.change.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 该注解用于生成doc文档
 * 该注解用于在包的注解
 * 该注解尽在运行期间有效
 * @author xinhua
 *
 */
@Documented
@Target(ElementType.PACKAGE)
@Retention(RetentionPolicy.RUNTIME)
public @interface PackageAnno
{

    String desc() default "我是描述包的注解";
}
