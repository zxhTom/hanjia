package tom.change.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 该注解用于doc文档
 * 该注解用于类的构造函数的注解
 * 该注解尽在运行期间有效
 * @author xinhua
 *
 */
@Documented
@Target(ElementType.CONSTRUCTOR)
@Retention(RetentionPolicy.RUNTIME)
public @interface ConstructorAnno
{

    String decs() default "我是描述构造函数的注解";
}
