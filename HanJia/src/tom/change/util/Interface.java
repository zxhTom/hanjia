package tom.change.util;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class Interface {

	private Interface(){
		
	}
	
	public static List<Class<?>> getAllClassByInterface(Class<?> c){
		List<Class<?>> list=new ArrayList<Class<?>>();
		if(c.isInterface()){
			//获取当前包名
			String packName=c.getPackage().getName();
		}
		return list;
	}
	
	public static List<Class<?>> getClasses(String packName) throws IOException{
		List<Class<?>> list=new ArrayList<Class<?>>();
		//获取类构造器
		ClassLoader loader=Thread.currentThread().getContextClassLoader();
		//将包名路径更改为/类型
		packName=packName.replace(".", "/");
		//根据路径获取类资源
		Enumeration<URL> resources=loader.getResources(packName);
		return list;
	}
}
