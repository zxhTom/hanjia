package tom.change.m22j;

import java.util.Date;

public class User {

	private String name;
	
	private Date birth;
	
	private Integer age;
	
	private boolean sex;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	
	public boolean isSex() {
		return sex;
	}

	public void setSex(boolean sex) {
		this.sex = sex;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", birth=" + birth + ", age=" + age
				+ ", sex=" + sex + "]";
	}

	
	
	
}
