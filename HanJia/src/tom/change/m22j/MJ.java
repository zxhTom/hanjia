package tom.change.m22j;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Map.Entry;


public class MJ {
	//实体
	private static Object object;
	//map集合
	private static Map<String, Object> paramMap;
	
	public static void init(Object obj,Map<String, Object> map){
		object=obj;
		paramMap=map;
	}
	public static Object currentObject(){
		return object;
	}
	/**
	 * 将map集合转换成事先准备的java实体
	 * @return
	 * @throws Exception
	 */
	public static Object MapToJava() throws Exception{
		if(object==null){
			throw new Exception("请先配置转换的实体类");
		}
		Class<?> clazz=object.getClass();
		Object instance = clazz.newInstance();
		//获取实体类的所在位置
		String location = clazz.getName();
		Class<?> target = Class.forName(location);
		for(Entry<String, Object> entry:paramMap.entrySet()){
			Field field=clazz.getDeclaredField(entry.getKey());
			field.setAccessible(true);
			field.set(instance, entry.getValue());
		}
		System.out.println(instance);
		return instance;
	}

	public static Map<String, Object> JavaToMap() throws IllegalArgumentException, IllegalAccessException, InstantiationException{
		Object instance = object.getClass().newInstance();
		Field[] fields = object.getClass().getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			paramMap.put(field.getName().toString(), field.get(instance));
		}
		return paramMap;
	}
}
