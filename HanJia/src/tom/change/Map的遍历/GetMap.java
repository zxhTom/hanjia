package tom.change.Map的遍历;

import java.util.HashMap;
import java.util.Map;

public class GetMap {

	private GetMap(){}
	
	public static Map<String, Object> get(){
		Map<String, Object> map=new HashMap<String,Object>();
		map.put("test1", "test1");
		map.put("test2", "test2");
		map.put("test3", "test3");
		map.put("test4", "test4");
		map.put("test5", "test5");
		map.put("test6", "test6");
		return map;
	}
}
