package tom.change.Map的遍历.通过entryset;

import java.util.Map;
import java.util.Map.Entry;

import tom.change.Map的遍历.GetMap;

public class EntrySet2 {

	 public static void main(String[] args){
		 Map<String, Object> map = GetMap.get();
		 for (Entry<String, Object> entry : map.entrySet()) {
			System.out.println(entry.getKey()+"@@@"+entry.getValue());
		}
	 }
}
