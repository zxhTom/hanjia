package tom.change.Map的遍历.通过entryset;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import tom.change.Map的遍历.GetMap;

public class EntrySet {

	public static void main(String[] args){
		Map<String, Object> map = GetMap.get();
		Set<Entry<String, Object>> entrySet = map.entrySet();
		Iterator<Entry<String, Object>> iterator = entrySet.iterator();
		while(iterator.hasNext()){
			Entry<String, Object> next = iterator.next();
			System.out.println(next.getKey()+"@@@@"+next.getValue());
		}
	}
}
