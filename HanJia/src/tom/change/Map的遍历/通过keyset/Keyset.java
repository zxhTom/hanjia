package tom.change.Map的遍历.通过keyset;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import tom.change.Map的遍历.GetMap;

public class Keyset {

	public static void main(String[] args){
		Map<String, Object> map = GetMap.get();
		//通过keyset获取map集合种的所有key
		Set<String> keySet = map.keySet();
		for (String key : keySet) {
			System.out.println(key+"@@@"+map.get(key));
		}
	}
}
