package tom.change.Reflect.获取父类和所有接口;

public class Test {

	public static void main(String[] args) throws ClassNotFoundException{
		Class<?> target=Class.forName("tom.change.Reflect.获取父类和所有接口.Dog");
		//获取Dog类的父类
		Class<?> parent=target.getSuperclass();
		//获取该类继承的所有类
		Class<?> intefaces[]=target.getInterfaces();
		System.out.println("Dog 父类 ： "+parent);
		System.out.println("....................................");
		System.out.println("接口：");
		for (int i = 0; i < intefaces.length; i++) {
			System.out.println(i+"@:@"+intefaces[i]);
		}
	}
}
