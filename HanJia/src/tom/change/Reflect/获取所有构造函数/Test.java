package tom.change.Reflect.获取所有构造函数;

import java.lang.reflect.Constructor;

public class Test {

	public static void main(String[] args) throws ClassNotFoundException{
		
		Class<?> controll=Class.forName("tom.change.Reflect.获取所有构造函数.Tuser");
		Constructor<?> constructors[] = controll.getConstructors();
		System.out.println(controll.getName()+"的所有构造函数：");
		for (int i = 0; i < constructors.length; i++) {
			System.out.println(constructors[i]);
			//获取构造函数的参数的类型，可以自己构造
		    Class<?> type[] = constructors[i].getParameterTypes();
		    for (int j = 0; j < type.length; j++) {
				System.out.println(type[j].getName());
			}
		}
	}
}
