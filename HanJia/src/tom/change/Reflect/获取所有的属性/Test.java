package tom.change.Reflect.获取所有的属性;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class Test {

	public static void main(String[] args) throws ClassNotFoundException{
		Class<?> clazz=Class.forName("tom.change.Reflect.获取父类和所有接口.Dog");
		//获取本地类的属性，父类和接口的没有
		Field[] fields = clazz.getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			//获取字段的修饰权限符
			System.out.println(Modifier.toString(fields[i].getModifiers())+"@@@"+fields[i].getName());
		}
	}
}
