package tom.change.Reflect;

/**
 * 实例化 class 对象的记住那个方式
 * @author Chirs
 *
 */
public class GetRealClass {

	public static void main(String[] args) throws ClassNotFoundException{
		Class<?> first=null;
		Class<?> second=null;
		Class<?> third=null;
		//一般采用这种形式
		first=Class.forName("tom.change.Reflect.GetRealClass");
		second=new GetRealClass().getClass();
		third=GetRealClass.class;
		System.out.println("first Class : " +first);
		System.out.println("second Class : " + second);
		System.out.println("third Class : " + third);
	}
}
