package tom.change.Reflect.设置字段;

import java.lang.reflect.Field;

public class Test {

	//userl类有个userCode字段，private 但是没有set方法，我们如何通过反射来操作这个字段呢
	public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException, SecurityException, InstantiationException, IllegalAccessException{
		Class<?> clazz=Class.forName("tom.change.Reflect.设置字段.Tuser");
		Object object=clazz.newInstance();
		Field field = clazz.getDeclaredField("userCode");
		field.setAccessible(true);
		field.set(object, "21131084");
		System.out.println(field.get(object));
		Tuser user=(Tuser)object;
		System.out.println(user.getUserCode());
	}
}
