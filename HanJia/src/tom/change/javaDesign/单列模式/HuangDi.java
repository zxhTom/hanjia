package tom.change.javaDesign.单列模式;

public class HuangDi {

	private static HuangDi huangDi=null;
	
	private HuangDi(){
		
	}
	public static HuangDi getInstance(){
		if(huangDi==null){
			huangDi=new HuangDi();
		}
		return huangDi;
	}
	public static void getHuangDiInfos(){
		System.out.println("朱元璋皇帝");
	}
}
