package tom.change.javaDesign.单列模式;
@SuppressWarnings("all")	
public class Minister {

	public static void main(String[] args){
		HuangDi huangDi1=HuangDi.getInstance();
		huangDi1.getHuangDiInfos();
		HuangDi huangDi2=HuangDi.getInstance();
		huangDi2.getHuangDiInfos();
		HuangDi huangDi3=HuangDi.getInstance();
		huangDi3.getHuangDiInfos();
		System.out.println("*************************");
		System.out.println(huangDi1);
		System.out.println(huangDi2);
		System.out.println(huangDi3);
	}
}
