package tom.change.javaDesign.代理模式;

public class Wangpo implements Women{

	private Women women;
	public Wangpo(){
		women=new Panjinlian();
	}
	public Wangpo(Women women){
		this.women=women;
	}
	@Override
	public void makeEyesToMan() {
		// TODO Auto-generated method stub
		women.makeEyesToMan();
	}

	@Override
	public void happyWithMan() {
		// TODO Auto-generated method stub
		women.happyWithMan();
	}

}
