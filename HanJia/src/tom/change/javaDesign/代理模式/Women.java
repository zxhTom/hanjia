package tom.change.javaDesign.代理模式;
/**
 * 定义一种类型的女人，王婆和潘金莲都属于这种类型的女人
 * @author Chirs
 *
 *具有的功能：向男人抛媚眼
 *		       和男人。。。。。
 */
public interface Women {

	public void makeEyesToMan();
	public void happyWithMan();
}
