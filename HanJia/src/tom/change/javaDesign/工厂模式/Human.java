package tom.change.javaDesign.工厂模式;

public interface Human {

	//人总是会笑的
	public void laugh();
	//人也是会哭的
	public void cry();
	//人还会聊天
	public void talk();
	
}
