package tom.change.javaDesign.工厂模式;

public class BlackHuman implements Human{

	@Override
	public void laugh() {
		// TODO Auto-generated method stub
		System.out.println("黑种人在笑");
	}

	@Override
	public void cry() {
		// TODO Auto-generated method stub
		System.out.println("黑种人在哭");
	}

	@Override
	public void talk() {
		// TODO Auto-generated method stub
		System.out.println("黑种人在聊天");
	}

}
