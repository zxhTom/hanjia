package tom.change.javaDesign.工厂模式;

import tom.change.util.Interface;

public class HumanFactory {

	public static Human createHuman(Class<?> c){
		Human human=null;
		try {
			human=(Human) Class.forName(c.getName()).newInstance();
		} catch (InstantiationException  e) {
			// TODO: handle exception
			System.out.println("必须制定一个颜色人种");
		} catch (IllegalAccessException e) {
			// TODO: handle exception
			System.out.println("人种定义出错");
		} catch (ClassNotFoundException e) {
			// TODO: handle exception
			System.out.println("你指定的人种我是真的找不到啊");
		}
		return human;
	}
}
