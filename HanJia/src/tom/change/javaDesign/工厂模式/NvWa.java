package tom.change.javaDesign.工厂模式;

import java.io.IOException;

import tom.change.util.Interface;

public class NvWa {

	public static void main(String[] args){
		try {
			Interface.getClasses("tom.change.javaDesign.代理模式");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("造出的第一批人是白人：");
		Human whiteHuman=HumanFactory.createHuman(WhiteHuman.class);
		whiteHuman.laugh();
		whiteHuman.cry();
		whiteHuman.talk();
		System.out.println("造出的第二批人是黑人：");
		Human blackHuman=HumanFactory.createHuman(BlackHuman.class);
		blackHuman.laugh();
		blackHuman.cry();
		blackHuman.talk();
		System.out.println("造出的第三批人是黄人：");
		Human yellowHuman=HumanFactory.createHuman(YellowHuman.class);
		yellowHuman.laugh();
		yellowHuman.cry();
		yellowHuman.talk();
	}
}
