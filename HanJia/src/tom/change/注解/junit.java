package tom.change.注解;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;

public class junit
{

    public static void main(String[] args)
    {
        /*Method[] declaredMethods = IReqApi.class.getDeclaredMethods();
        for (Method method : declaredMethods) {
            System.out.println("method:@@@"+method);
            Annotation[]  methodAnnotations = method.getAnnotations();
            System.out.println("方法上的注解");
            for (Annotation annotation : methodAnnotations)
            {
                System.out.println(annotation);
            }
            Annotation[][] parameterAnnotationsArray = method.getParameterAnnotations();
            System.out.println("参数上的注解");
            for (Annotation[] annotations : parameterAnnotationsArray)
            {
                for (Annotation annotation : annotations)
                {
                    System.out.println(annotation);
                }
            }
            
        }*/
        IReqApi api = create(IReqApi.class);
        api.login("zhangxinhua", "password123");
    }
    public static <T> T create(final Class<T> service){
        return (T) Proxy.newProxyInstance(service.getClassLoader(), new Class<?>[]{service}, new InvocationHandler()
        {
            
            @Override
            public Object invoke(Object proxy, Method method, Object[] args)
                    throws Throwable
            {
                System.out.println("动态代理开始运行");
                ReqType reqType = method.getAnnotation(ReqType.class);
                System.out.println("IReqApi---reqType->" + (reqType.reqType() == ReqType.ReqTypeEnum.POST ? "POST" : "OTHER"));
                ReqUrl reqUrl = method.getAnnotation(ReqUrl.class);
                System.out.println("IReqApi---reqUrl->" + reqUrl.reqUrl());
                Type[] parameterTypes = method.getGenericParameterTypes();
                Annotation[][] parameterAnnotationsArray = method.getParameterAnnotations();//拿到参数注解
                for (int i = 0; i < parameterAnnotationsArray.length; i++) {
                    Annotation[] annotations = parameterAnnotationsArray[i];
                    if (annotations != null) {
                        ReqParam reqParam = (ReqParam) annotations[0];
                        System.out.println("reqParam---reqParam->" + reqParam.value() + "==" + args[i]);
                    }
                }
                //下面就可以执行相应的网络请求获取结果 返回结果
                String result = "";//这里模拟一个结果

                return result;
            }
        });
    }
}
