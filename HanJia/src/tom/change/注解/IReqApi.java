package tom.change.注解;

import tom.change.注解.ReqType.ReqTypeEnum;

public interface IReqApi
{
    @ReqType(reqType=ReqTypeEnum.POST)
    @ReqUrl(reqUrl="http://www.baidu.com")
    String login(@ReqParam(value="userId") String userId,@ReqParam(value="pwd") String pwd);
    
    String sayHello();
}
